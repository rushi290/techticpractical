<?php
namespace Techtic\Practical\Controller\Index;

class Test extends \Magento\Framework\App\Action\Action
{
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute()
	{
		echo "Title : Coming from Techtic Practical custom module";
		?>
		<img src="<?php echo $this->getViewFileUrl('Techtic_Practical::images/crop.png'); ?>" />
		<?php

		exit;
	}
}