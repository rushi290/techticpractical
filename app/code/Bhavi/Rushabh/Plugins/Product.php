<?php
namespace Bhavi\Rushabh\Plugins;

class Product
{
    public function aftergetName(\Magento\Catalog\Model\Product $product, $name){
        $price = $product->getData('price');
        // print $price;
        // die();
        if($price<300){
            $name .= " so cheap";
        }
        
        else{
            $name .= " so expensive";
        }
        return $name;
    }
}