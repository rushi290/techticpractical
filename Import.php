<?php
$file = fopen('var/import/customfileupload.csv', 'r', '"'); // set path to the CSV file
if ($file !== false)
{
  
    require __DIR__ . '/app/bootstrap.php';
    $bootstrap = \Magento\Framework\App\Bootstrap::create(BP, $_SERVER);
    $objectManager = $bootstrap->getObjectManager();
    $state = $objectManager->get('Magento\Framework\App\State');
    $state->setAreaCode('adminhtml');
    $stockRegistry = $objectManager->get('Magento\CatalogInventory\Api\StockRegistryInterface');

    $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/import-product.log');
    $logger = new \Zend\Log\Logger(); 
    $logger->addWriter($writer);

    $header = fgetcsv($file); // get data headers and skip 1st row
    $required_data_fields = 3;

    while ($row = fgetcsv($file, 3000, ",") )
    {
        $data_count = count($row);
        if ($data_count < 1)
        {
            continue;
        }
        $product = $objectManager->create('Magento\Catalog\Model\Product');         
        $data = array();
        $data = array_combine($header, $row);

        $sku = $data['sku'];
        if ($data_count < $required_data_fields)
        {
            $logger->info("Skipping product sku " . $sku . ", not all required fields are present to create the product.");
            continue;
        }

        $name = $data['name'];
        $description = $data['description'];
        $shortDescription = $data['short_description'];
        $qty = trim($data['qty']);
        $price = trim($data['price']);

        try
        { 
            // $objectManager = \Magento\Framework\App\ObjectManager::getInstance(); 
            // //print_r($objectManager);die;
            // // Add Images To The Product
            // $imagePath = "http://localhost/demostore/var/import/images"; // path of the image
            
            // $product->addImageToMediaGallery($imagePath, array('image', 'small_image', 'thumbnail'), false, false);
            
            // //$product->save();
            

            $product->setTypeId('simple') // product type
                    ->setStatus(1) // 1 = enabled
                    ->setAttributeSetId(4)
                    ->setName($name)
                    ->setSku($sku)
                    ->setPrice($price)
                    ->setTaxClassId(1) // 0 = None
                    ->setCategoryIds(array(3)) // array of category IDs, 2 = Default Category
                    ->setDescription($description)
                    ->setShortDescription($shortDescription)
                    ->setWebsiteIds(array(1)) // Default Website ID
                    ->setStoreId(0) // Default store ID
                    ->setVisibility(4) // 4 = Catalog & Search
                    ->save();
                 }
        catch (\Exception $e)
        {
            $logger->info('Error importing product sku: '.$sku.'. '.$e->getMessage());
            continue;
        }
        try
        {
            $stockItem = $stockRegistry->getStockItemBySku($sku);

            if ($stockItem->getQty() != $qty)
            {
                $stockItem->setQty($qty);
                if ($qty > 0)
                {
                    $stockItem->setIsInStock(1);
                }
                $stockRegistry->updateStockItemBySku($sku, $stockItem);
            }
        }
        catch (\Exception $e)
        {
            $logger->info('Error importing stock for product sku: '.$sku.'. '.$e->getMessage());
            continue;
        }
        unset($product);
    }
    fclose($file);
}

?>
